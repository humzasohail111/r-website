import React from "react";
import image from "../Images/Tech.jpg";

const Card = (props) => {
  return (
    <>
      <div className="card">
        <img src={props.image} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">{props.title}</h5>
          <p className="card-text">{props.para}</p>
          <a href="https://picsum.photos/200/300" className="btn btn-primary">
            {props.btn}
          </a>
        </div>
      </div>
    </>
  );
};

export default Card;
