import React from "react";

const CardJ = [
  {
    image: "https://picsum.photos/seed/picsum/150",
    title: "Card title",
    para: "Some quick example text to build on the card title and make up the bulk of the card's content./",
    btn: "Go Somewhere",
  },
  {
    image: "https://picsum.photos/seed/picsum/150",
    title: "Card title",
    para: "Some quick example text to build on the card title and make up the bulk of the card's content./",
    btn: "Go Somewhere",
  },
  {
    image: "https://picsum.photos/seed/picsum/150",
    title: "Card title",
    para: "Some quick example text to build on the card title and make up the bulk of the card's content./",
    btn: "Go Somewhere",
  },
  {
    image: "https://picsum.photos/seed/picsum/150",
    title: "Card title",
    para: "Some quick example text to build on the card title and make up the bulk of the card's content./",
    btn: "Go Somewhere",
  },
  {
    image: "https://picsum.photos/seed/picsum/150",
    title: "Card title",
    para: "Some quick example text to build on the card title and make up the bulk of the card's content./",
    btn: "Go Somewhere",
  },
  {
    image: "https://picsum.photos/seed/picsum/150",
    title: "Card title",
    para: "Some quick example text to build on the card title and make up the bulk of the card's content./",
    btn: "Go Somewhere",
  },
];

export default CardJ;
