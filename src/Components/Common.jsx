import React from "react";
import Navbar from "./Navbar/Navbar.jsx";
import Tech from "../Images/Tech.jpg";
import { NavLink } from "react-router-dom";
import "../Components/CSS/Header.css";
import CardJ from "./CardJ.jsx";

const Common = (props) => {
  return (
    <>
      <section id="header" className="d-flex align-items-center">
        <div className="container nav-big">
          <div className="row">
            <div className="col-10 mx-auto">
              <div className="row">
                <div className="col-md-6 pt-10 pt-lg-0 order-2 order-lg-1 d-flex justify-content-center flex-column">
                  <h1>
                    {props.heading}
                    <strong className="brand-name"> {props.name}</strong>
                  </h1>
                  <h2 className="my-3">{props.para}</h2>
                  <div className="mt-3">
                    <NavLink to="/services" className="btn-get-started">
                      {props.btn}
                    </NavLink>
                  </div>
                </div>
                <div className="col-lg-6 order-1 order-lg-2 header-img">
                  <img
                    src={props.pic}
                    className="img-fluid animated"
                    alt="Home img"
                  ></img>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Common;
