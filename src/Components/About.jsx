import React from "react";
import Common from "./Common.jsx";
import image from "../Images/Tech.jpg";

const About = () => {
  return (
    <>
      <Common
        heading="Grow your business with"
        name="Humza Sohail"
        para="We are the team of talented developer making websites"
        visit="/about"
        btn="btn-started"
        pic={image}
      />
    </>
  );
};

export default About;
