import React from "react";
import Navbar from "./Navbar/Navbar.jsx";
import Tech from "../Images/Tech.jpg";
import { NavLink } from "react-router-dom";
import "../Components/CSS/Header.css";
import Common from "./Common.jsx";
import image from "../Images/Tech.jpg";

const Home = () => {
  return (
    <>
      <Common
        heading="Grow your business with"
        name="Humza Sohail"
        para="We are the team of talented developer making websites"
        visit="/home"
        btn="btn-started"
        pic={image}
      />
    </>
  );
};

export default Home;
