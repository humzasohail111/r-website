import React from "react";
import image from "../Images/Tech.jpg";
import Card from "./Card";
import CardJ from "./CardJ";

const Services = () => {
  return (
    <>
      <div className="my-5">
        <h1 className="text-center">Our Services</h1>
      </div>
      <div className="container-fluid mb-5">
        <div className="row">
          <div className="col-10 mx-auto">
            <div className="row">
              {CardJ.map((val, index) => {
                return (
                  <>
                    <div className="col-md-4 col-10 mx-auto mt-5">
                      <Card
                        image={val.image}
                        title={val.title}
                        para={val.para}
                        btn={val.btn}
                      />
                    </div>
                  </>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Services;
